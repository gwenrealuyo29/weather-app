from django.forms import ModelForm, Select
from .models import City

class CityForm(ModelForm):
    class Meta:
        model = City
        fields = ['name']
        city_choices = [('', 'Select a city')]
        
        for city in City.objects.all():
            city_choices.append((city, city))

        widgets = {'name' : Select(attrs={'class' : 'input', 'placeholder' : 'City Name'}, choices=city_choices)}