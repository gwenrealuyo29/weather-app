from django.http import HttpResponseNotFound
from django.shortcuts import render
import requests
from .forms import CityForm

def index(request):
    try:
        url = 'http://api.openweathermap.org/data/2.5/weather?q={}&units=metric&appid=591eff98224c6d051693901f287bb0d5'
        city = 'Taguig'

        # Selecting a city
        if request.method == 'POST':
            form = CityForm(request.POST)
            if form.is_valid():
                city = request.POST['name']
            else:
                return render(request, 'weather/weather.html', {'form': form})

        form = CityForm()

        # Retrieving data from API
        r = requests.get(url.format(city)).json()
        
        if r['cod'] != 404:
            city_weather = {
                'city' : city,
                'temperature' : r['main']['temp'],
                'description': r['weather'][0]['description'],
                'icon':  r['weather'][0]['icon'],
            }

            context = { 'city_weather' : city_weather, 'form' : form }

            return render(request, 'weather/weather.html', context)
        else:
            return HttpResponseNotFound("City not found")
    except:
        return HttpResponseNotFound("Page not found")